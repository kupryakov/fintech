//
//  LoadImageServiceStub.swift
//  fintechTests
//
//  Created by Влад Купряков on 08/12/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

@testable import fintech

final class LoadImageServiceForStub: ILoadImageService {
    private let imageCore: INetwork
    
    init(imageCore: INetwork) {
        self.imageCore = imageCore
    }
    
    func loadImageUrls(completion: @escaping (([ImageUrls]?, Error?) -> Void)) {
        
    }
    var stub: [String:Data]? = [:]
    func loadImage(type: String, imageUrl: ImageUrls, completion: @escaping (([String : Data]?, Error?) -> Void)) {
        self.imageCore.requestImage(type: type, imageUrls: imageUrl, completion: {[weak self](imageDict, error)  in
            if error != nil {
                completion(nil, error)
            } else if let imageDict = imageDict {
                self?.stub = imageDict
                completion(imageDict, nil)
            }
        })
    }
}
