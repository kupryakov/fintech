//
//  fintechTests.swift
//  fintechTests
//
//  Created by Влад Купряков on 06/12/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import XCTest

@testable import fintech

class fintechTests: XCTestCase {
    
    var loadImageService: ILoadImageService!
    var networkMock: NetworkMock!
    
    var loadImageServiceForStub: LoadImageServiceForStub!
    var network: INetwork!
    
    override func setUp() {
        networkMock = NetworkMock()
        loadImageService = LoadImageService(imageCore: networkMock)
        
        network = Network(session: URLSession.shared)
        loadImageServiceForStub = LoadImageServiceForStub(imageCore: network)
    }
    
    override func tearDown() {
        super.tearDown()
        networkMock = nil
        loadImageService = nil
        
        network = nil
        loadImageServiceForStub = nil
    }
    
    func testOnLoadImageThatLoadImageServiceCalledNetwork() {
        //given
        let url = "https://moe.shikimori.org/system/animes/original/33352.jpg?1534617802"
        let downloadType = "preview"
        let imgUrls = ImageUrls(previewURL: url, largeImageURL: url)
        
        //when
        loadImageService.loadImage(type: downloadType, imageUrl: imgUrls, completion: {_, _ in})
        
        //then
        XCTAssert(networkMock.isSendCalled)
    }
    
    func testOnLoadImageThatAfterNetworkCallbackCalledloadImageServiceCallbackWithImageData() {
        //given
        let url = "https://moe.shikimori.org/system/animes/original/33352.jpg?1534617802"
        let imgUrls = ImageUrls(previewURL: url, largeImageURL: url)
        let downloadType = "preview"
        let expectation = self.expectation(description: "LoadImg")
        
        //when
        loadImageServiceForStub.loadImage(type: downloadType, imageUrl: imgUrls) { (imgDict, err) in
            expectation.fulfill()
        }
        
        //then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(loadImageServiceForStub.stub?.values.count == 1)
    }
}
