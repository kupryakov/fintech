//
//  NetworkMock.swift
//  fintechTests
//
//  Created by Влад Купряков on 06/12/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

@testable import fintech

final class NetworkMock: INetwork {
    func requestImagesUrl(completion: @escaping ([ImageUrls]?, Error?) -> Void) {
        
    }
    
    var isSendCalled = false
    
    func requestImage(type: String, imageUrls: ImageUrls, completion: @escaping (([String : Data]?, Error?) -> Void)) {
        isSendCalled = true
    }
}

