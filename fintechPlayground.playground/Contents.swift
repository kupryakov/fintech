import UIKit
//import fintechUI
import PlaygroundSupport

class Company {
    
    var CEO: ceo!
    var prodMan: productManager!
    var firstDeveloper: developer1!
    var secondDeveloper: developer2!
    
    init(){
        CEO = ceo()
        prodMan = productManager()
        firstDeveloper = developer1()
        secondDeveloper = developer2()
        CEO.company = self
        CEO.manager = prodMan
        prodMan.ceo = CEO
        prodMan.firstDev = firstDeveloper
        prodMan.secondDev = secondDeveloper
        firstDeveloper.productManager = prodMan
        secondDeveloper.productManager = prodMan
    }
    
    func printFullCompany(){
        print("Company: ")
        print("Company CEO: ", CEO.ceoName)
        print("Company manager: ", prodMan.prodManagerName)
        print("Company developer1: ", firstDeveloper.developer1)
        print("Company developer2: ", secondDeveloper.developer2, "\n")
    }
    
    func createCompany() -> Company{
        let company = Company()
        
        company.CEO.dialogWithManager(message: "Oh my")
        
        company.firstDeveloper.communicateWithManager(message: "Продукт-менеджер, дай ТЗ")
        company.firstDeveloper.communicateWithManager(message: "Продукт-менеджер, дай мне новую задачу")
        
        company.firstDeveloper.communicationWithCeo(message: "CEO, я хочу зарплату больше")
        
        company.secondDeveloper.communicateWithManager(message: "prodMan, day TZ")
        company.secondDeveloper.communicateWithManager(message: "ProdMan, day novuyu zadachu")
        
        company.secondDeveloper.communicationWithCeo(message: "ceo, hochu dengi")
        
        company.firstDeveloper.messageToOtherDev(message: "Ti govnokoder")
        company.secondDeveloper.messageToOtherDev(message: "Ya otpravil pull request")
        
        return company
    }
    
    class ceo {
        
        weak var manager: productManager!
        weak var company: Company!
        
        var ceoName = "Boris"
        
        lazy var printCompany: ()->() = { [unowned self] in
            self.company.printFullCompany()
        }
        
        lazy var printManager: ()->() = { [unowned self] in
            self.manager.printProdMan()
        }
        
        lazy var printDeveloreps: ()->() = { [unowned self] in
            self.manager.firstDev.printFirstDev()
            self.manager.secondDev.printSecondDev()
        }
        
        func ceoListeningDev(developer: String, message: String) {
            print(developer, " say to \(ceoName): ", message)
        }
        
        func dialogWithManager(message: String) {
            manager.managerListeningCeo(message: message)
            printCompany()
        }
        
        deinit{
            print("ceo rip")
        }
    }
    
    class productManager{
        
        weak var ceo: ceo!
        weak var firstDev: developer1!
        weak var secondDev: developer2!
        
        var prodManagerName = "Ivan"
        
        func managerListeningCeo(message: String) {
            print("\(ceo.ceoName) say to \(prodManagerName): ", message)
        }
        
        func developersTalks(developer: String, message: String){
            switch developer {
            case firstDev.developer1:
                secondDev.messageFromOtherDev(devName: developer, message: message)
                break
            case secondDev.developer2:
                firstDev.messageFromOtherDev(devName: developer, message: message)
                break
            default:
                break
            }
        }
        
        func managerListening(developer: String, message: String) {
            print(developer, " say to \(prodManagerName): ", message)
        }
        
        func developerMessageToCEO(devName: String, message: String){
            ceo.ceoListeningDev(developer: devName, message: message)
        }
        
        func printProdMan(){
            print("Product Manager: ", prodManagerName)
        }
        
        deinit{
            print("productManager rip")
        }
    }
    class developer1{
        weak var productManager: productManager!
        
        var developer1 = "Oleg"
        
        func communicateWithManager(message: String) {
            productManager.managerListening(developer: developer1, message: message)
        }
        
        func communicationWithCeo(message: String) {
            productManager.developerMessageToCEO(devName: developer1, message: message)
        }
        
        func printFirstDev(){
            print("Developer 1: ", developer1)
        }
        
        func messageFromOtherDev(devName: String, message: String){
            print(devName, "say to \(developer1): ", message)
        }
        
        func messageToOtherDev(message: String){
            productManager.developersTalks(developer: developer1, message: message)
        }
        
        deinit{
            print("developer1 rip")
        }
        
    }
    
    class developer2{
        weak var productManager: productManager!
        
        var developer2 = "Petr"
        
        func communicateWithManager(message: String) {
            productManager.managerListening(developer: developer2, message: message)
        }
        
        func communicationWithCeo(message: String) {
            productManager.developerMessageToCEO(devName: developer2, message: message)
        }
        
        func printSecondDev(){
            print("Developer 2: ", developer2)
        }
        
        func messageFromOtherDev(devName: String, message: String){
            print(devName, "say to \(developer2): ", message)
        }
        
        func messageToOtherDev(message: String){
            productManager.developersTalks(developer: developer2, message: message)
        }
        
        deinit{
            print("developer2 rip")
        }
        
    }
    deinit{
        print("company rip")
    }
}



var company: Company! = Company()
company.createCompany()
company = nil
