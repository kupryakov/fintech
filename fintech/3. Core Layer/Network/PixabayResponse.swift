//
//  PixabayResponse.swift
//  fintech
//
//  Created by Влад Купряков on 22/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

struct PixabayResponse: Codable {
    var hits: [ImageUrls]
}
