//
//  Network.swift
//  fintech
//
//  Created by Влад Купряков on 22/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

protocol INetwork {
    func requestImagesUrl(completion: @escaping ([ImageUrls]?, Error?) -> Void)
    func requestImage(type: String, imageUrls: ImageUrls, completion: @escaping (([String:Data]?,Error?) -> Void))
}

private let urlString = "https://pixabay.com/api/?key=10769270-b23926fbd1a1b91c2718436c0&q=yellow+flowers&image_type=photo&per_page=200"

class Network: INetwork {
    
    let session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }
    
    func requestImagesUrl(completion: @escaping ([ImageUrls]?, Error?) -> Void) {
        guard let url = URL(string: urlString) else { fatalError() }
        let request = URLRequest(url: url)
        let dataTask = session.dataTask(with: request) { (data,response,error) in
            if error != nil {
                completion(nil, error)
            } else if let data = data {
                let pixabayResponse = try? JSONDecoder().decode(PixabayResponse.self, from: data)
                completion(pixabayResponse?.hits, nil)
            }
        }
        dataTask.resume()
    }
    
    func requestImage(type: String, imageUrls: ImageUrls, completion: @escaping (([String:Data]?,Error?) -> Void)) {
        var imageStr = ""
        if type == "preview" {
            guard let imageString = imageUrls.previewURL else { return }
            imageStr = imageString
        } else if type == "full" {
            guard let imageString = imageUrls.largeImageURL else { return }
            imageStr = imageString
        }
        guard let url = URL(string: imageStr) else { fatalError() }
        let request = URLRequest(url: url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion(nil, error)
            } else if let imageData = data {
                guard let fullImageUrl = imageUrls.largeImageURL else { return }
                completion([fullImageUrl:imageData], nil)
            }
        }
        dataTask.resume()
    }
}
