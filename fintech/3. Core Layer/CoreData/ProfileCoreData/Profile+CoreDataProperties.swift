//
//  Profile+CoreDataProperties.swift
//  fintech
//
//  Created by Влад Купряков on 01/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var name: String?
    @NSManaged public var descr: String?
    @NSManaged public var image: NSData?

}
