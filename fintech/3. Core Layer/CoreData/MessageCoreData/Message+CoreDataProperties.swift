//
//  Message+CoreDataProperties.swift
//  fintech
//
//  Created by Влад Купряков on 08/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//
//

import Foundation
import CoreData


extension Message {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Message> {
        return NSFetchRequest<Message>(entityName: "Message")
    }
    @NSManaged public var mine: Bool
    @NSManaged public var date: NSDate
    @NSManaged public var identifier: String
    @NSManaged public var text: String
    @NSManaged public var conversation: Conversation?

}
