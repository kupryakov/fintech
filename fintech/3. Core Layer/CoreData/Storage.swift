//
//  File.swift
//  fintech
//
//  Created by Влад Купряков on 01/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation
import CoreData

protocol IStorage : class {
    func saveProfile(profile: ProfileModel)
    func loadProfile()
    func saveDialog(dialogList: [ConversationListCellModel])
    func takeDialogs() -> [ConversationListCellModel]
    func saveMessages(user: String, messagesId:String, message: String, messageIsMine: Bool, messageDate: Date)
    func takeMessages(user: String) -> [ConversationCellModel]
    var delegate: IProfileService? { get set }
    var mainManagedObjectContext: NSManagedObjectContext { get }
    var privateManagedObjectContext: NSManagedObjectContext { get }
//    func loadProfileMulti() -> String
}

class Storage: IStorage {
    
   weak var delegate: IProfileService?
    
    lazy var applicationDocumentsDirectory: NSURL = {
        if let path = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true).first {
            if !FileManager.default.fileExists(atPath: path) {
                do {
                    if let url = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first {
                    try FileManager.default.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil)
                    }
                } catch let error as NSError {
                    print(error.localizedDescription);
                }
            }
        }
        guard let url = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first else {return NSURL()}
        return url as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        if let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd") {
            guard let object = NSManagedObjectModel(contentsOf: modelURL) else {return NSManagedObjectModel()}
            return object
        }
        return NSManagedObjectModel()
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Model.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            print("Error with persistent store")
        }
        return coordinator
    }()
    
    lazy var privateManagedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    lazy var mainManagedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    func saveMessages(user: String, messagesId:String, message: String, messageIsMine: Bool, messageDate: Date) {
        let predicate = NSPredicate(format: "userName == %@", user)
        let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
        fetchRequest.predicate = predicate
        do {
                let result = try privateManagedObjectContext.fetch(fetchRequest)
                let mes = NSEntityDescription.insertNewObject(forEntityName: String(describing: Message.self), into: privateManagedObjectContext) as? Message
                mes?.text = message
                mes?.date = messageDate as NSDate
                mes?.mine = messageIsMine
                mes?.identifier = messagesId
                mes?.conversation = result.first
                do {
                    try privateManagedObjectContext.save()
                }
                catch let error {
                    print(error.localizedDescription)
                }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func takeMessages(user: String) -> [ConversationCellModel] {
        do {
            let predicate = NSPredicate(format: "userName == %@", user)
            let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
            fetchRequest.predicate = predicate
            let result = try privateManagedObjectContext.fetch(fetchRequest)
            var messagesList: [ConversationCellModel] = []
            if let object = result.first {
                for item in object.message {
                    messagesList.append(ConversationCellModel(date: item.date as Date, mine: item.mine, identifier: item.identifier, text: item.text))
                }
            }
            messagesList.sort(by: { date1, date2 in date1.date < date2.date })
            return messagesList
        } catch let error {
            print(error.localizedDescription)
        }
        return []
    }
    
    func saveDialog(dialogList: [ConversationListCellModel]) {
            do {
                for item in dialogList {
                    let predicate = NSPredicate(format: "userName == %@", item.userName)
                    let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
                    fetchRequest.predicate = predicate
                    let result = try privateManagedObjectContext.fetch(fetchRequest)
                    if result.count == 0 {
                        let newDialog = NSEntityDescription.insertNewObject(forEntityName:String(describing: Conversation.self), into: privateManagedObjectContext) as? Conversation
                        newDialog?.online = item.online
                        newDialog?.date = item.date as NSDate
                        newDialog?.lastMessage = item.lastMessage
                        newDialog?.userName = item.userName
                        newDialog?.unreadMes = item.unreadMes
                        newDialog?.conversationId = item.conversationId
                                do {
                                    try privateManagedObjectContext.save()
                                }
                                catch let error {
                                    print(error.localizedDescription)
                                }
                    } else {
                        result.first?.online = item.online
                        result.first?.date = item.date as NSDate
                        result.first?.lastMessage = item.lastMessage
                        result.first?.userName = item.userName
                        result.first?.unreadMes = item.unreadMes
                        result.first?.conversationId = item.conversationId
                        do {
                            try privateManagedObjectContext.save()
                        }
                        catch let error {
                            print(error.localizedDescription)
                        }
                    }
                }
                
            }
            catch let error {
                print(error.localizedDescription)
            }
    }
    
    func takeDialogs() -> [ConversationListCellModel] {
        do {
            let request = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
            let result = try privateManagedObjectContext.fetch(request)
            var usersList: [ConversationListCellModel] = []
            
            for item in result {
                usersList.append(ConversationListCellModel(conversationId: item.conversationId, lastMessage: item.lastMessage, date: item.date as Date, userName: item.userName, unreadMes: item.unreadMes, online: item.online))
            }
            return usersList
        }
        catch let error{
            print(error.localizedDescription)
        }
        return []
    }
    
    func saveProfile(profile: ProfileModel) {
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Profile.self))
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            do {
                try privateManagedObjectContext.execute(deleteRequest)
                try privateManagedObjectContext.save()
            }
            catch let error {
                print(error.localizedDescription)
            }
            let profileEntity = NSEntityDescription.insertNewObject(forEntityName:String(describing: Profile.self), into: privateManagedObjectContext) as? Profile
            profileEntity?.name = profile.name
            profileEntity?.descr = profile.description
            profileEntity?.image = profile.image as NSData?
            do {
                try privateManagedObjectContext.save()
                DispatchQueue.main.async {
                    self.delegate?.alert(value: true)
                }
            }
            catch let error {
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    self.delegate?.alert(value: false)
                }
            }
    }
    
    func loadProfile() {
            do {
                let request = NSFetchRequest<Profile>(entityName: String(describing: Profile.self))
                let result = try privateManagedObjectContext.fetch(request)
                if !result.isEmpty {
                    let profile = ProfileModel(name: result.first?.name, description: result.first?.descr, image: result.first?.image as Data?)
                    DispatchQueue.main.async {
                        self.delegate?.receive(profile: profile)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.delegate?.receive(profile: ProfileModel())
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    
//    func loadProfileMulti() -> String {
//        var userName = ""
//        do {
//            let request = NSFetchRequest<Profile>(entityName: String(describing: Profile.self))
//            let result = try mainManagedObjectContext.fetch(request)
//            if !result.isEmpty {
//                let profile = ProfileModel(name: result.first?.name, description: result.first?.descr, image: result.first?.image as Data?)
//                    self.delegate?.receive(profile: profile)
//                    guard let name = result.first?.name else {return ""}
//                    userName = name
//                    return userName
//            }
//        } catch let error {
//            print(error.localizedDescription)
//        }
//        return userName
//    }
}
