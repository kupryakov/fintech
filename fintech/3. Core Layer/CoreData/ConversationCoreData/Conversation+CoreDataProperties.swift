//
//  Conversation+CoreDataProperties.swift
//  fintech
//
//  Created by Влад Купряков on 08/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//
//

import Foundation
import CoreData


extension Conversation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Conversation> {
        return NSFetchRequest<Conversation>(entityName: "Conversation")
    }
    @NSManaged public var conversationId: String
    @NSManaged public var online: Bool
    @NSManaged public var unreadMes: Bool
    @NSManaged public var userName: String
    @NSManaged public var date: NSDate
    @NSManaged public var lastMessage: String
    @NSManaged public var message: Set<Message>
}
