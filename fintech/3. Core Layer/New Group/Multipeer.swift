//
//  ICommunicationService.swift
//  fintech
//
//  Created by Влад Купряков on 25/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import MultipeerConnectivity

protocol IMultipeerConnection {
    var delegate: CommunicationServiceDelegate? { get set }
    var online: Bool { get set }
    func send(_ message: Messages, to peer: Peer)
//    func startSess(userName: String)
}

protocol CommunicationServiceDelegate: class {
    /// Browsing
    func communicationService(_ communicationService: IMultipeerConnection,
                              didFoundPeer peer: Peer)
    func communicationService(_ communicationService: IMultipeerConnection,
                              didLostPeer peer: Peer)
    func communicationService(_ communicationService: IMultipeerConnection,
                              didNotStartBrowsingForPeers error: Error)
    /// Advertising
    func communicationService(_ communicationService: IMultipeerConnection,
                              didReceiveInviteFromPeer peer: Peer,
                              invintationClosure: (Bool) -> Void)
    func communicationService(_ communicationService: IMultipeerConnection,
                              didNotStartAdvertisingForPeers error: Error)
    /// Messages
    func communicationService(_ communicationService: IMultipeerConnection,
                              didReceiveMessage message: Messages,
                              from peer: Peer)
}

class MultipeerConnection: NSObject, IMultipeerConnection {
    
    private var myPeerId = MCPeerID(displayName: "revolt")
    private var serviceType = "tinkoff-chat"
    private let discoveryInfo = ["userName": "kupryakov"]
    private var sessions: [String: MCSession] = [:]
    
    private var serviceAdvertiser : MCNearbyServiceAdvertiser
    private var serviceBrowser : MCNearbyServiceBrowser
    
    
    weak var delegate: CommunicationServiceDelegate?
    
    var online: Bool = false
    
//    private let storage: IStorage = Storage()
    
    override init() {
        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerId, discoveryInfo: discoveryInfo, serviceType: self.serviceType)
        self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerId, serviceType: self.serviceType)
        super.init()
        self.serviceAdvertiser.delegate = self
        self.serviceAdvertiser.startAdvertisingPeer()
        
        online = true
        
        self.serviceBrowser.delegate = self
        self.serviceBrowser.startBrowsingForPeers()
//        startSess(userName: self.storage.loadProfileMulti())
    }
    
//    func startSess(userName: String) {
//        if (userName != "") {
//            myPeerId = MCPeerID(displayName: userName)
//            self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerId, discoveryInfo: discoveryInfo, serviceType: self.serviceType)
//            self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerId, serviceType: self.serviceType)
//
//            self.serviceAdvertiser.delegate = self
//            self.serviceAdvertiser.startAdvertisingPeer()
//
//            self.serviceBrowser.delegate = self
//            self.serviceBrowser.startBrowsingForPeers()
//        } else {
//            self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerId, discoveryInfo: discoveryInfo, serviceType: self.serviceType)
//            self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerId, serviceType: self.serviceType)
//
//            self.serviceAdvertiser.delegate = self
//            self.serviceAdvertiser.startAdvertisingPeer()
//
//            self.serviceBrowser.delegate = self
//            self.serviceBrowser.startBrowsingForPeers()
//        }
//    }
    
    func sessionWith(_ peerID: MCPeerID) -> MCSession {
        if let session = sessions[peerID.displayName] {
            return session
        } else {
            let session = MCSession(peer: self.myPeerId, securityIdentity: nil, encryptionPreference: .none)
            session.delegate = self
            sessions.updateValue(session, forKey: peerID.displayName)
            return session
        }
    }
    
    func send(_ message: Messages, to peer: Peer) {
        if let session = sessions[peer.identifier] {
        do {
            print(session.connectedPeers)
            for item in session.connectedPeers {
                if item.displayName == peer.identifier {
                    try session.send(Data(message.text.utf8), toPeers: [item], with: .reliable)
                }
            }
        }
        catch let error {
            print(error)
            }
        }
    }
    deinit {
        self.serviceAdvertiser.stopAdvertisingPeer()
        self.serviceBrowser.stopBrowsingForPeers()
    }
}

extension MultipeerConnection : MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        delegate?.communicationService(self, didNotStartAdvertisingForPeers: error)
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
                invitationHandler(true, sessionWith(peerID))
        delegate?.communicationService(self, didReceiveInviteFromPeer: Peer(identifier: peerID.displayName, name: "userName"), invintationClosure: {(true) -> Void in })
    }
    
}
extension MultipeerConnection : MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        delegate?.communicationService(self, didLostPeer: Peer(identifier: peerID.displayName, name: "userName"))
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        print("invite peer: \(peerID.displayName)")
        browser.invitePeer(peerID, to: sessionWith(peerID), withContext: nil, timeout: 10)
        guard let info = info?["userName"] else {return}
        delegate?.communicationService(self, didFoundPeer: Peer(identifier: peerID.displayName, name: info))
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        delegate?.communicationService(self, didNotStartBrowsingForPeers: error)
    }
    
}


extension MultipeerConnection : MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            print("connected: \(peerID)")
        case .connecting:
            print("connecting: \(peerID)")
        case.notConnected:
            print("notConnected: \(peerID)")
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        guard let message = String(data: data, encoding: .utf8) else { return }
        let messageString = message
        var dict: [String: String] = [:]
        if let data = messageString.data(using: .utf8)  {
            do {
                guard let data = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String] else {return}
                dict = data
                if let messageId = dict["messageId"] {
                    guard var text = dict["text"] else {return}
                    if text == "" {
                        text = " "
                    }
                    let message = Messages(identifier: messageId, text: text)
                    delegate?.communicationService(self, didReceiveMessage: message, from: Peer(identifier: peerID.displayName, name: "userName"))
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
}


