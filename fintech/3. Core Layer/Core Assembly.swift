//
//  Core Assembly.swift
//  fintech
//
//  Created by Влад Купряков on 16/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation
protocol ICoreAssembly : class {
    var storageCore: IStorage { get set }
    var multipeerCore: IMultipeerConnection { get }
    var networkCore: INetwork { get }
}

class CoreAssembly: ICoreAssembly {
    lazy var storageCore: IStorage = Storage()
    lazy var multipeerCore: IMultipeerConnection = MultipeerConnection()
    lazy var networkCore: INetwork = Network(session: URLSession.shared)
}
