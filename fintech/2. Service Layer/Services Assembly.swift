//
//  Services Assembly.swift
//  fintech
//
//  Created by Влад Купряков on 16/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

protocol IServicesAssembly : class {
    var fetchResults: IFetchResults { get }
    var fetchedResultsControllerService: IFetchResultController { get }
    var communicationServise: ICommunicationService { get  set }
    var profileService: IProfileService { get set }
    var imageService: ILoadImageService { get set }
    var imageResizeService: IImageResizeService { get }
    var animationTouchesService: IAnimationTouchesService { get }
    func setupStorage()
}

class ServicesAssembly: IServicesAssembly {
    private var coreAssembly: ICoreAssembly
    
    init(coreAssembly: ICoreAssembly) {
        self.coreAssembly = coreAssembly
    }
    
    lazy var fetchResults: IFetchResults = FetchResults()
    lazy var fetchedResultsControllerService: IFetchResultController = FetchedResultsControllerFactory(storage: coreAssembly.storageCore)
    lazy var communicationServise: ICommunicationService = CommunicationService(storageDataProtocol: coreAssembly.storageCore, multipeer: coreAssembly.multipeerCore)
    lazy var profileService: IProfileService = ProfileService(servicesAssembly: self, storageDelegate: coreAssembly.storageCore)
    lazy var imageService: ILoadImageService = LoadImageService(imageCore: coreAssembly.networkCore)
    lazy var imageResizeService: IImageResizeService = ImageResizeService()
    lazy var animationTouchesService: IAnimationTouchesService = AnimationTouchesService()
    func setupStorage() {
        coreAssembly.storageCore.delegate = profileService
    }
}
