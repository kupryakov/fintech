//
//  ImageResizeService.swift
//  fintech
//
//  Created by Влад Купряков on 23/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit

protocol IImageResizeService {
    func scaleImage(image: UIImage, size: Int) -> UIImage
}
class ImageResizeService: IImageResizeService {
    func scaleImage(image: UIImage, size: Int) -> UIImage {
        let scale: CGFloat = 0.0
        UIGraphicsBeginImageContextWithOptions(CGSize(width: size, height: size), true, scale)
        image.draw(in: CGRect(origin: .zero, size: CGSize(width: size, height: size)))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = scaledImage else {return UIImage()}
        return image
    }
}
