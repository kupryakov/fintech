//
//  FetchedResultsController.swift
//  fintech
//
//  Created by Влад Купряков on 08/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation
import CoreData

protocol IFetchResultController {
    func messageFetchResultController(user: String) -> NSFetchedResultsController<Conversation>
    func conversationFetchResultController(managedObjectContext: String) -> NSFetchedResultsController<Conversation>
}

class FetchedResultsControllerFactory: NSObject {
    private let storage: IStorage
    
    init(storage: IStorage) {
        self.storage = storage
    }
}

extension FetchedResultsControllerFactory: IFetchResultController {
    func messageFetchResultController(user: String) -> NSFetchedResultsController<Conversation> {
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        let predicate = NSPredicate(format: "userName == %@", user)
        let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.resultType = .managedObjectResultType
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: storage.privateManagedObjectContext, sectionNameKeyPath: nil, cacheName: nil)

        return controller
    }
    
    func conversationFetchResultController(managedObjectContext: String) -> NSFetchedResultsController<Conversation> {
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        let predicate = NSPredicate(format: "online == true")
        let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = predicate
        fetchRequest.resultType = .managedObjectResultType
        if managedObjectContext == "main" {
            let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: storage.mainManagedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            return controller
        } else {
            let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: storage.privateManagedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            return controller
        }
    }
}
