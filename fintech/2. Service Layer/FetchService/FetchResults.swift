//
//  FetchResults.swift
//  fintech
//
//  Created by Влад Купряков on 10/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation
import CoreData

protocol IFetchResults {
    func takeConversation(conversationId: String, managedObjectContext: NSManagedObjectContext)
    func takeConversationsWithOnlineUsers(managedObjectContext: NSManagedObjectContext)
    func takeMessagesFromConversation(conversationId: String, managedObjectContext: NSManagedObjectContext)
    func takeOnlineUsers(managedObjectContext: NSManagedObjectContext)
    func takeUserById(userId: String, managedObjectContext: NSManagedObjectContext)
}

class FetchResults {
    
}

extension FetchResults: IFetchResults {
    
    func takeConversation(conversationId: String, managedObjectContext: NSManagedObjectContext) {
        do {
            let predicate = NSPredicate(format: "conversationId == %@", conversationId)
            let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
            fetchRequest.predicate = predicate
            let result = try managedObjectContext.fetch(fetchRequest)
            print("CONVERSATION BY ID: \n", result)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func takeConversationsWithOnlineUsers(managedObjectContext: NSManagedObjectContext) {
        do {
            let predicate = NSPredicate(format: "online == true AND lastMessage != %@ ", "")
            
            let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
            fetchRequest.predicate = predicate
            let result = try managedObjectContext.fetch(fetchRequest)
            
            print("CONVERSATIONS WITH ONLINE USERS: \n", result)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func takeMessagesFromConversation(conversationId: String, managedObjectContext: NSManagedObjectContext) {
        do {
            let predicate = NSPredicate(format: "conversationId == %@", conversationId)
            let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
            fetchRequest.predicate = predicate
            fetchRequest.resultType = .managedObjectResultType
            let result = try managedObjectContext.fetch(fetchRequest)
            var messagesList: [ConversationCellModel] = []
            if let object = result.first {
                for item in object.message {
                    messagesList.append(ConversationCellModel(date: item.date as Date, mine: item.mine, identifier: item.identifier, text: item.text))
                }
            }
            messagesList.sort(by: { date1, date2 in date1.date < date2.date })
            print("MESSAGES FROM CONVERSATION BY ID: \n", messagesList)
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func takeOnlineUsers(managedObjectContext: NSManagedObjectContext) {
        do {
            let predicate = NSPredicate(format: "online == true")
            let fetchRequest = NSFetchRequest<NSDictionary>(entityName: String(describing: Conversation.self))
            fetchRequest.propertiesToFetch = ["userName"]
            fetchRequest.predicate = predicate
            fetchRequest.resultType = .dictionaryResultType
            let result = try managedObjectContext.fetch(fetchRequest)
            
            print("ONLINE USERS: \n", result)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func takeUserById(userId: String, managedObjectContext: NSManagedObjectContext) {
        do {
            let predicate = NSPredicate(format: "userName == %@", userId)
            let fetchRequest = NSFetchRequest<NSDictionary>(entityName: String(describing: Conversation.self))
            fetchRequest.propertiesToFetch = ["userName"]
            fetchRequest.predicate = predicate
            fetchRequest.resultType = .dictionaryResultType
            let result = try managedObjectContext.fetch(fetchRequest)
            
            print("USER BY ID: \n", result)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
