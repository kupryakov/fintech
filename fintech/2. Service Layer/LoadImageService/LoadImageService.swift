//
//  ImageService.swift
//  fintech
//
//  Created by Влад Купряков on 22/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

protocol ILoadImageService {
    func loadImageUrls(completion: @escaping (([ImageUrls]?, Error?) -> Void))
    func loadImage(type: String, imageUrl: ImageUrls, completion: @escaping (([String:Data]?, Error?) -> Void))
}

class LoadImageService: ILoadImageService {
    private let imageCore: INetwork
    
    init(imageCore: INetwork) {
        self.imageCore = imageCore
    }
    
    func loadImageUrls(completion: @escaping (([ImageUrls]?, Error?) -> Void)) {
        DispatchQueue.global().async {
            self.imageCore.requestImagesUrl(completion: {(imageUrls, error) in
                if error != nil {
                    completion(nil, error)
                } else if let imageUrls = imageUrls {
                completion(imageUrls, nil)
                }
            })
        }
    }
    
    func loadImage(type: String, imageUrl: ImageUrls, completion: @escaping (([String:Data]?, Error?) -> Void)) {
        self.imageCore.requestImage(type: type, imageUrls: imageUrl, completion: {(imageDict, error) in
            if error != nil {
                completion(nil, error)
            } else if let imageDict = imageDict {
                completion(imageDict, nil)
            }
        })
    }
}
