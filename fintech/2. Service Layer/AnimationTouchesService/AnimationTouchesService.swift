//
//  Touches.swift
//  fintech
//
//  Created by Влад Купряков on 30/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit
@objc protocol IAnimationTouchesService: class {
    func handleLongTap(_ sender: UILongPressGestureRecognizer)
    func handleTap(_ sender: UITapGestureRecognizer)
}

class AnimationTouchesService: IAnimationTouchesService {
    
    private var lastPosition: CGPoint = CGPoint(x: 0.0, y: 0.0)
    private var imageView: UIImageView = UIImageView()
    private let imageSize: CGFloat = 50
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        guard let view = sender.view else { return }
        animateTinkoff(position: sender.location(in: sender.view), view: view)
        sender.cancelsTouchesInView = false
   }
    
    @objc func handleLongTap(_ sender: UILongPressGestureRecognizer) {
        sender.minimumPressDuration = 0.1
        let location = sender.location(in: sender.view)
        
        if lastPosition != location {
            imageView.removeFromSuperview()
            
            imageView = UIImageView(frame: CGRect(x: location.x - imageSize/2, y: location.y - imageSize/2, width: imageSize, height: imageSize))
            imageView.image = UIImage(named: "Tinkoff")
            guard let view = sender.view else { return }
            repeatAnimations(position: location, view: view, imageView: imageView)
        }
        
        if sender.state == .changed {
            guard let view = sender.view else { return }
            animateTinkoff(position: sender.location(in: sender.view), view: view)
        }
    
        if sender.state == .ended {
            imageView.removeFromSuperview()
            guard let view = sender.view else { return }
            animateTinkoff(position: sender.location(in: sender.view), view: view)
        }
    }
    
    func animateTinkoff(position: CGPoint, view: UIView) {
        let imageView = UIImageView(frame: CGRect(x: position.x - imageSize/2, y: position.y - imageSize/2, width: imageSize, height: imageSize))
        imageView.image = UIImage(named: "Tinkoff")
        view.addSubview(imageView)
        UIView.animateKeyframes(withDuration: 1.0, delay: 0.0, animations: {
            imageView.center.y += 50
        }, completion: { val in
            imageView.removeFromSuperview()
        })
    }
    
    func repeatAnimations(position: CGPoint, view: UIView, imageView: UIImageView) {
        view.addSubview(imageView)
        UIView.animateKeyframes(withDuration: 0.5, delay: 0.0, options: [.repeat], animations: {
            imageView.center.y += 50
        }, completion: nil)
    }
}
