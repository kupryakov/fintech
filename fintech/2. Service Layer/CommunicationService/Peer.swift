//
//  User.swift
//  fintech
//
//  Created by Влад Купряков on 25/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

struct Peer {
    let identifier: String
    let name: String
}
