//
//  Message.swift
//  fintech
//
//  Created by Влад Купряков on 25/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

struct Messages {
    enum `Type`: String {
        case textMessage = "TextMessage"
    }
    let identifier: String
    let text: String
}
