//
//  Communication.swift
//  fintech
//
//  Created by Влад Купряков on 25/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

protocol ICommunicationService {
    func sendMessage(text: String, user: String)
    func updateConversation(online: Bool?,lastMessage: String?,date: Date?, user: String,unreadMes: Bool?)
    var updateConversationDelegate: IConversation? { get set }
    var updateConversationListDelegate: IConversationList? { get set }
}

class CommunicationService: ICommunicationService {
    
    weak var updateConversationDelegate: IConversation?
    weak var updateConversationListDelegate: IConversationList?
    
    private let storageDataProtocol: IStorage
    private var multipeer: IMultipeerConnection
    
    private var conversations: [ConversationListCellModel] = []
    
    init(storageDataProtocol: IStorage, multipeer: IMultipeerConnection) {
        self.storageDataProtocol = storageDataProtocol
        self.multipeer = multipeer
        self.multipeer.delegate = self
        makeAllPeersOffline()
    }
    
    private func makeDate() -> Date {
        let date = Date()
        return date
    }
    
    private func generateRandomId() -> String {
        guard let string = "\(arc4random_uniform(UINT32_MAX))+\(Date.timeIntervalSinceReferenceDate)+\(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString() else {return "300"}
        return string
    }
    
    private func makeAllPeersOffline() {
        var newDialogList: [ConversationListCellModel] = []
        let dialogList = storageDataProtocol.takeDialogs()
        if !dialogList.isEmpty {
            for item in dialogList {
                newDialogList.append(ConversationListCellModel(conversationId: item.conversationId, lastMessage: item.lastMessage, date: item.date, userName: item.userName, unreadMes: item.unreadMes, online: false))
            }
        }
        storageDataProtocol.saveDialog(dialogList: newDialogList)
    }
    
    private func foundNewPeer(user: String) {
        if !conversations.contains(where: { $0.userName == user}) {
            var lastMes = ""
            var conversationId = generateRandomId()
            var unreadMes = false
            var date = Date(timeIntervalSince1970: 0)
            let request = storageDataProtocol.takeDialogs()
            if !request.isEmpty {
                for item in request {
                    if item.userName == user {
                        lastMes = item.lastMessage
                        date = item.date
                        conversationId = item.conversationId
                        unreadMes = item.unreadMes
                    }
                }
            }
            let userCell = ConversationListCellModel(conversationId: conversationId, lastMessage: lastMes, date: date, userName: user, unreadMes: unreadMes, online: true)
            conversations.append(userCell)
        } else {
                updateConversation(online: true, lastMessage: nil, date: nil, user: user, unreadMes: nil)
        }
    }
    
    func sendMessage(text: String, user: String) {
        let messageId = generateRandomId()
        let dict = ["eventType": "TextMessage", "messageId": "\(messageId)", "text": "\(text)"]
        do {
            let messageData = try JSONEncoder().encode(dict)
            guard let messageJson = String(data: messageData, encoding: .utf8) else {return}
            let messageJSON = messageJson
            let message = Messages(identifier: messageId, text: messageJSON)
            let date = makeDate()
            updateConversation(online: nil, lastMessage: text, date: date, user: user, unreadMes: nil)
            storageDataProtocol.saveMessages(user: user, messagesId: message.identifier, message: text, messageIsMine: true, messageDate: date)
            storageDataProtocol.saveDialog(dialogList: conversations)
            multipeer.send(message, to: Peer(identifier: user, name: "userName"))
            updateConversationDelegate?.didReceiveUpdates()
            updateConversationListDelegate?.didReceiveUpdates()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func updateConversation(online: Bool?,lastMessage: String?,date: Date?, user: String,unreadMes: Bool?) {
        if !conversations.isEmpty {
            for index in 0...conversations.count-1 {
                if conversations[index].userName == user {
                    if let online = online {
                        conversations[index].online = online
                    }
                    if let lastMessage = lastMessage {
                        conversations[index].lastMessage = lastMessage
                    }
                    if let date = date {
                        conversations[index].date = date
                    }
                    if let unreadMes = unreadMes {
                        conversations[index].unreadMes = unreadMes
                    }
                }
            }
        }
        storageDataProtocol.saveDialog(dialogList: conversations)
        updateConversationListDelegate?.didReceiveUpdates()
    }
}


extension CommunicationService: CommunicationServiceDelegate {
    
    func communicationService(_ communicationService: IMultipeerConnection, didFoundPeer peer: Peer) {
        foundNewPeer(user: peer.identifier)
        
        storageDataProtocol.saveDialog(dialogList: conversations)
        updateConversationDelegate?.companionIsOnline(value: true, userName: peer.identifier)
        updateConversationListDelegate?.didReceiveUpdates()
    }
    
    func communicationService(_ communicationService: IMultipeerConnection, didLostPeer peer: Peer) {
        updateConversation(online: false, lastMessage: nil, date: nil, user: peer.identifier, unreadMes: nil)
        
        storageDataProtocol.saveDialog(dialogList: conversations)
        updateConversationDelegate?.companionIsOnline(value: false, userName: peer.identifier)
        updateConversationListDelegate?.didReceiveUpdates()
    }
    
    func communicationService(_ communicationService: IMultipeerConnection, didNotStartBrowsingForPeers error: Error) {
        
    }
    
    func communicationService(_ communicationService: IMultipeerConnection, didReceiveInviteFromPeer peer: Peer, invintationClosure: (Bool) -> Void) {
        print("receive invite form \(peer.identifier)")
    }
    
    func communicationService(_ communicationService: IMultipeerConnection, didNotStartAdvertisingForPeers error: Error) {
        
    }
    
    func communicationService(_ communicationService: IMultipeerConnection, didReceiveMessage message: Messages, from peer: Peer) {
        let date = makeDate()
        
        updateConversation(online: nil, lastMessage: message.text, date: date, user: peer.identifier, unreadMes: true)
        
        storageDataProtocol.saveMessages(user: peer.identifier, messagesId: message.identifier, message: message.text, messageIsMine: false, messageDate: date)
        storageDataProtocol.saveDialog(dialogList: conversations)
       
        updateConversationDelegate?.didReceiveUpdates()
        updateConversationListDelegate?.didReceiveUpdates()
    }
}
