//
//  ProfileService.swift
//  fintech
//
//  Created by Влад Купряков on 17/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

protocol IProfileService : class {
    func loadProfile()
    func save(profile: ProfileModel)
    func receive(profile: ProfileModel)
    func alert(value: Bool)
    var delegate: IProfile? { get set }
}

class ProfileService: IProfileService {
    
    private var storageAssembly: IStorage
    private var servicesAssembly: IServicesAssembly
    
    weak var delegate: IProfile?
    
    init(servicesAssembly: IServicesAssembly, storageDelegate: IStorage) {
        self.storageAssembly = storageDelegate
        self.servicesAssembly = servicesAssembly
    }
    
    func loadProfile() {
        servicesAssembly.setupStorage()
        storageAssembly.loadProfile()
    }
    
    func save(profile: ProfileModel) {
        servicesAssembly.setupStorage()
        storageAssembly.saveProfile(profile: profile)
    }
    
    func receive(profile: ProfileModel) {
        delegate?.receive(profile: profile)
    }
    
    func alert(value: Bool) {
        delegate?.alert(value: value)
    }
}
