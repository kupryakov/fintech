//
//  UserList.swift
//  fintech
//
//  Created by Влад Купряков on 27/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

struct ConversationListCellModel {
    var conversationId: String
    var lastMessage: String
    var date: Date
    var userName: String
    var unreadMes: Bool
    var online: Bool
}
