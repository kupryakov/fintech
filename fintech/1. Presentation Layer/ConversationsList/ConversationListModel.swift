//
//  ConversationListModel.swift
//  fintech
//
//  Created by Влад Купряков on 18/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation
import CoreData

struct ConversationListModel {
    var userID: String?
    var fetchedResultsController: NSFetchedResultsController<Conversation>?
    let section = ["Online"]
    let identifier = String(describing: ConversationListCell.self)
}
