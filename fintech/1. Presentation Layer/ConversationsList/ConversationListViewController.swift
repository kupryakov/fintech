//
//  ViewController.swift
//  fintech
//
//  Created by Влад Купряков on 03/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit
import CoreData

protocol IConversationList : class {
    func didReceiveUpdates()
    func uploadName() -> String
}

class ConversationListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private var conversationListModel = ConversationListModel()
    
    private let presentationAssembly: IPresentationAssembly
    private let conversationfetchedResultsControllerService: IFetchResultController
    private let animationTouchesService: IAnimationTouchesService
    init(presentationAssembly: IPresentationAssembly, conversationfetchedResultsControllerService: IFetchResultController, animationTouchesService: IAnimationTouchesService) {
        self.presentationAssembly = presentationAssembly
        self.conversationfetchedResultsControllerService = conversationfetchedResultsControllerService
        self.animationTouchesService = animationTouchesService
        super.init(nibName: nil, bundle: nil)
        navigationItem.title = "Tinkoff Chat"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.view.addGestureRecognizer(UITapGestureRecognizer(target: animationTouchesService, action: #selector(animationTouchesService.handleTap(_:))))
        self.navigationController?.view.addGestureRecognizer(UILongPressGestureRecognizer(target: animationTouchesService, action: #selector(animationTouchesService.handleLongTap(_:))))
        tableView.register(UINib(nibName: conversationListModel.identifier, bundle: nil), forCellReuseIdentifier: conversationListModel.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        createBarButton()
        conversationListModel.fetchedResultsController?.delegate = self
        tableView.separatorColor = UIColor.clear
    }
    
    private func createBarButton() {
        let image = UIImage(named: "ProfileIcon")?.withRenderingMode(.alwaysOriginal)
        let rightButtonItem = UIBarButtonItem(
            image: image,
            style: .plain,
            target: self,
            action: #selector(showProfile(sender: ))
        )
        self.navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    private func createDate(date: Date) -> String {
        let currentDate = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let currentYear = calendar.component(.year, from: currentDate)
        let currentMonth = calendar.component(.month, from: currentDate)
        let currentDay = calendar.component(.day, from: currentDate)
        
        if year == 1970 {
            return ""
        }
        if currentYear > year {
            return "\(day)/\(month)"
        }
        if currentYear == year {
            if currentMonth > month {
                return "\(day)/\(month)"
            }
            if currentMonth == month {
                if currentDay > day {
                    return "\(day)/\(month)"
                }
                if currentDay == day {
                    if minutes < 10 {
                        return "\(hour):0\(minutes)"
                    }
                    return "\(hour):\(minutes)"
                }
            }
        }
        return ""
    }
    
    @objc func showProfile(sender: UIBarButtonItem) {
            let navigationViewController = UINavigationController(rootViewController: presentationAssembly.profileView())
            self.present(navigationViewController, animated: true, completion: nil)
    }
}

//MARK: - UITableViewDataSource
extension ConversationListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            guard let sections = conversationListModel.fetchedResultsController?.sections else {return 0}
            return sections[section].numberOfObjects
        case 1:
            return 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: conversationListModel.identifier, for: indexPath) as? ConversationListCell else {
                return UITableViewCell()
            }
            switch indexPath.section {
            case 0:
                if let model = conversationListModel.fetchedResultsController?.object(at: indexPath) {
                    if model.online == true {
                        cell.backgroundColor = UIColor(red: 250/255, green: 255/255, blue: 222/255, alpha: 0.8)
                        cell.titleLabel.text = model.userName
                        cell.dateLabel.text = createDate(date: model.date as Date)
                        if model.lastMessage == "" {
                            cell.messageLabel.text = "No messages yet"
                            cell.messageLabel.font = UIFont(name: "Noteworthy", size: 15)
                        } else {
                            cell.messageLabel.text = model.lastMessage
                            if model.unreadMes == true {
                                cell.messageLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
                            } else {
                                cell.messageLabel.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
                            }
                        }
                    }
                }
        break
        case 1:
            if let model = conversationListModel.fetchedResultsController?.object(at: indexPath) {
                if model.online == false {
                    cell.backgroundColor = UIColor.clear
                    cell.titleLabel.text = model.userName
                    cell.dateLabel.text = createDate(date: model.date as Date)
                    if model.lastMessage == "" {
                        cell.messageLabel.text = "No messages yet"
                        cell.messageLabel.font = UIFont(name: "Noteworthy", size: 15)
                    } else {
                        cell.messageLabel.text = model.lastMessage
                        if model.unreadMes == true {
                            cell.messageLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
                        } else {
                            cell.messageLabel.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.thin)
                        }
                    }
                }
            }
            break
        default:
            break
        }
            return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return conversationListModel.section[section]
    }
}
 
//MARK: - UITableViewDelegate

extension ConversationListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = conversationListModel.fetchedResultsController?.object(at: indexPath)
        switch indexPath.section {
        case 0:
            conversationListModel.userID = model?.userName
            break
        case 1:
            conversationListModel.userID = model?.userName
            break
        default:
            break
        }
        self.navigationController?.pushViewController(presentationAssembly.conversationViewController(), animated: true)
    }
}

extension ConversationListViewController: IConversationList {
    func didReceiveUpdates() {
        if navigationController?.visibleViewController == self {
            conversationListModel.fetchedResultsController = conversationfetchedResultsControllerService.conversationFetchResultController(managedObjectContext: "main")
        } else {
            conversationListModel.fetchedResultsController = conversationfetchedResultsControllerService.conversationFetchResultController(managedObjectContext: "private")
        }
        do {
            try conversationListModel.fetchedResultsController?.performFetch()
        } catch let error {
            print(error.localizedDescription)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func uploadName() -> String {
        guard let userName = conversationListModel.userID else { return ""}
        return userName
    }
}

extension ConversationListViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        guard let indexPath = indexPath else {return}
        
        switch type {
        case .update:
            tableView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let newIndexPath = newIndexPath else {return}
            tableView.moveRow(at: indexPath, to: newIndexPath)
        case .insert:

            tableView.insertRows(at: [indexPath], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
