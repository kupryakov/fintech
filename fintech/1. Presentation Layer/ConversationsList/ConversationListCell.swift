//
//  ConversationListCell.swift
//  fintech
//
//  Created by Влад Купряков on 03/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit

class ConversationListCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
