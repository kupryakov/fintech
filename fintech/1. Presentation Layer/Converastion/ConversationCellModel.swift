//
//  TrueMessage.swift
//  fintech
//
//  Created by Влад Купряков on 08/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

struct ConversationCellModel {
    let date: Date
    let mine: Bool
    let identifier: String
    let text: String
}
