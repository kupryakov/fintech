//
//  SecondCell.swift
//  fintech
//
//  Created by Влад Купряков on 06/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit

class SecondConversationViewCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureWithItem(item: ConversationCellModel) {
        messageLabel?.text = item.text
        messageLabel.layer.masksToBounds = true
        messageLabel.layer.cornerRadius = 5.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
