//
//  ConversationModel.swift
//  fintech
//
//  Created by Влад Купряков on 18/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct ConversationModel {
    let identifier = String(describing: ConversationViewCell.self)
    let identifier2 = String(describing: SecondConversationViewCell.self)
    var fetchedResultsController: NSFetchedResultsController<Conversation>?
    var messages: [ConversationCellModel] = []
    var companionIsOnline = true
    var label = UILabel(frame: CGRect(x:0, y:0, width: 130, height: 40))
}
