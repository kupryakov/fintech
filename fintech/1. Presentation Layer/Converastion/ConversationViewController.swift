//
//  ConversationViewController.swift
//  fintech
//
//  Created by Влад Купряков on 05/10/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit
import CoreData

protocol IConversation : class {
    func didReceiveUpdates()
    func companionIsOnline(value: Bool, userName: String)
}

class ConversationViewController: UIViewController, IConversation {
    @IBOutlet weak var conversationTableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var sendMessageButton: UIButton!
    
    @IBAction func sendMessageButton(_ sender: Any) {
        if let message = messageTextField.text, let user = conversationModel.label.text {
            communicationService.sendMessage(text: message, user: user)
        }
        messageTextField.text = ""
        sendMessageButtonAvailable(value: false)
    }
    
    private var conversationModel = ConversationModel()
    
    private let presentationAssembly: IPresentationAssembly
    private let communicationService: ICommunicationService
    private let fetchedResultsControllerService: IFetchResultController
    private var animationTouchesService: IAnimationTouchesService
    init(presentationAssembly: IPresentationAssembly, fetchedResultsControllerService: IFetchResultController, communicationService: ICommunicationService, animationTouchesService: IAnimationTouchesService) {
        self.presentationAssembly = presentationAssembly
        self.fetchedResultsControllerService = fetchedResultsControllerService
        self.communicationService = communicationService
        self.animationTouchesService = animationTouchesService
        super.init(nibName: nil, bundle: nil)
        setupNavigationTitle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        delegatesSetup()
        setupConversationView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        sendMessageButtonAvailable(value: false)
        guard let userName = conversationModel.label.text else { return }
        companionIsOnline(value: true, userName: userName)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let user = conversationModel.label.text {
            conversationModel.fetchedResultsController = fetchedResultsControllerService.messageFetchResultController(user: user)
            do {
                try conversationModel.fetchedResultsController?.performFetch()
                sortMessages()
                communicationService.updateConversation(online: nil, lastMessage: nil, date: nil, user: user, unreadMes: false)
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if let user = conversationModel.label.text {
            communicationService.updateConversation(online: nil, lastMessage: nil, date: nil, user: user, unreadMes: false)
        }
    }
    
    private func registerCells() {
        conversationTableView.register(UINib(nibName: conversationModel.identifier, bundle: nil), forCellReuseIdentifier: conversationModel.identifier)
        conversationTableView.register(UINib(nibName: conversationModel.identifier2, bundle: nil), forCellReuseIdentifier: conversationModel.identifier2)
    }
    
    private func setupConversationView() {
        self.navigationController?.view.addGestureRecognizer(UITapGestureRecognizer(target: animationTouchesService, action: #selector(animationTouchesService.handleTap(_:))))
        self.navigationController?.view.addGestureRecognizer(UILongPressGestureRecognizer(target: animationTouchesService, action: #selector(animationTouchesService.handleLongTap(_:))))
        messageTextField.addTarget(self, action: #selector(editMessageField(_:)), for: .editingChanged)
        sendMessageButton.isEnabled = false
        sendMessageButton.backgroundColor = .green
        sendMessageButton.layer.cornerRadius = 16
        sendMessageButton.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4)
        conversationTableView.rowHeight = UITableViewAutomaticDimension
        conversationTableView.separatorColor = UIColor.clear
    }
    
    private func delegatesSetup() {
        conversationTableView.dataSource = self
        conversationTableView.delegate = self
        messageTextField.delegate = self
    }
    
    private func sortMessages() {
        conversationModel.messages = []
        if let objects = conversationModel.fetchedResultsController?.fetchedObjects?.first {
            for item in objects.message {
                conversationModel.messages.append(ConversationCellModel(date: item.date as Date, mine: item.mine, identifier: item.identifier, text: item.text))
                }
        }
        conversationModel.messages.sort(by: { date1, date2 in date1.date < date2.date })
        DispatchQueue.main.async {
            self.conversationTableView?.reloadData()
            
            let numberOfRows = self.conversationModel.messages.count
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: 0)
                self.conversationTableView.scrollToRow(at: indexPath, at: .middle, animated: true)
            }
        }
    }
    
    @objc func editMessageField(_ sender: UITextField) {
        if sender.text != "" {
            if !sendMessageButton.isEnabled && conversationModel.companionIsOnline == true {
                sendMessageButtonAvailable(value: true)
            }
        } else {
            if sendMessageButton.isEnabled {
                sendMessageButtonAvailable(value: false)
            }
        }
    }
    
    private func sendMessageButtonAvailable(value: Bool) {
        if value == true && conversationModel.companionIsOnline == true {
            sendMessageButton.isEnabled = value
            UIView.animate(withDuration: 0.5, animations: {
                self.sendMessageButton.backgroundColor = .green
                self.sendMessageButton.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
            })
            UIView.animate(withDuration: 0.5, delay: 0.5, animations: {
                self.sendMessageButton.backgroundColor = .green
                self.sendMessageButton.transform = CGAffineTransform(scaleX: 1/1.15, y: 1/1.15)
            })
        } else if value == false || conversationModel.companionIsOnline == false {
            sendMessageButton.isEnabled = false
            UIView.animate(withDuration: 0.5, animations: {
                self.sendMessageButton.backgroundColor = .black
                self.sendMessageButton.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
            })
            UIView.animate(withDuration: 0.5, delay: 0.5, animations: {
                self.sendMessageButton.backgroundColor = .black
                self.sendMessageButton.transform = CGAffineTransform(scaleX: 1/1.15, y: 1/1.15)
            })
        }
    }
    
    private func setupNavigationTitle() {
        conversationModel.label.text = self.presentationAssembly.conversationListDelegate?.uploadName()
        conversationModel.label.textColor = UIColor.black
        conversationModel.label.font = UIFont.boldSystemFont(ofSize: 18)
        conversationModel.label.backgroundColor = UIColor.clear
        conversationModel.label.textAlignment = .center
        navigationItem.titleView = conversationModel.label
    }
    
    func companionIsOnline(value: Bool, userName: String) {
        if conversationModel.label.text == userName {
            conversationModel.companionIsOnline = value
            if value == false {
                UIView.animate(withDuration: 1.0, animations: {
                    self.conversationModel.label.transform = CGAffineTransform(scaleX: 1/1.1, y: 1/1.1)
                    self.conversationModel.label.textColor = UIColor.black
                })
                if sendMessageButton.isEnabled == true {
                    sendMessageButtonAvailable(value: false)
                }
            } else {
                UIView.animate(withDuration: 1.0, animations: {
                    self.conversationModel.label.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                    self.conversationModel.label.textColor = UIColor.green
                    })
                if messageTextField.text != "" {
                    sendMessageButtonAvailable(value: true)
                }
            }
        }
    }
    
    func didReceiveUpdates() {
        do {
            try conversationModel.fetchedResultsController?.performFetch()
            sortMessages()
        } catch let error {
            print(error.localizedDescription)
        }
    }
}


extension ConversationViewController: UITableViewDelegate {
    
}

extension ConversationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = conversationModel.fetchedResultsController?.fetchedObjects?.first?.message.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !conversationModel.messages.isEmpty {
            let model = conversationModel.messages[indexPath.row]
            if model.mine == true {
                if let cell = conversationTableView.dequeueReusableCell(withIdentifier: conversationModel.identifier2, for: indexPath) as? SecondConversationViewCell {
                    let mesConfig = ConversationCellModel(date: model.date, mine: model.mine, identifier: model.identifier, text: model.text)
                    cell.configureWithItem(item: mesConfig)
                    return cell
                }
            } else if model.mine == false {
                if let cell = conversationTableView.dequeueReusableCell(withIdentifier: conversationModel.identifier, for: indexPath) as? ConversationViewCell {
                    let mesConfig = ConversationCellModel(date: model.date, mine: model.mine, identifier: model.identifier, text: model.text)
                    cell.configureWithItem(item: mesConfig)
                    return cell
                }
            }
        }
        return UITableViewCell()
    }
}

extension ConversationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension ConversationViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        conversationTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        guard let indexPath = indexPath else {return}
        
        switch type {
        case .update:
            conversationTableView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let newIndexPath = newIndexPath else {return}
            conversationTableView.moveRow(at: indexPath, to: newIndexPath)
        case .insert:
            conversationTableView.insertRows(at: [indexPath], with: .automatic)
        case .delete:
            conversationTableView.deleteRows(at: [indexPath], with: .automatic)
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        conversationTableView.endUpdates()
    }
}
