//
//  ProfileModel.swift
//  fintech
//
//  Created by Влад Купряков on 18/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

struct ProfileModel {
    var name: String?
    var description: String?
    var image: Data?
}
