//
//  ViewController.swift
//  fintech
//
//  Created by Влад Купряков on 21.09.2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit

protocol IProfile : class {
    func receive(profile: ProfileModel)
    func alert(value: Bool)
    func pictureFromNetwork(image: UIImage)
}

class ProfileViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var chooseImageButton: UIButton!
    @IBOutlet weak var editedButton: UIButton!
    @IBOutlet weak var userProfileImage: UIImageView!

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var staticDescriptionLabel: UILabel!
    @IBOutlet weak var staticNameLabel: UILabel!
    
    @IBAction func editedButton(_ sender: Any) {
        saveButtonEnabled(value: false)
        editingMode(value: true)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        saveButtonEnabled(value: false)
        saveProfile()
    }
    
    @IBAction func chooseImageButton(_ sender: Any) {
        let alert = UIAlertController(title: "Фото профиля", message: "Выберите дальнейшее действие", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Установить из галлереи", style: .default , handler:{ (UIAlertAction)in
            self.pictureFromLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: "Сделать фото", style: .default , handler:{ (UIAlertAction)in
            self.pictureFromCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Загрузить фото", style: .default , handler:{ (UIAlertAction)in
            let navigationViewController = UINavigationController(rootViewController: self.presentationAssembly.imageView())
            self.present(navigationViewController, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        self.present(alert, animated: true, completion: {
        })
    }
    
    private let presentationAssembly: IPresentationAssembly
    private let profileService: IProfileService
    private let imageResizeService: IImageResizeService
    private let animationTouchesService: IAnimationTouchesService
    
    init(presentationAssembly: IPresentationAssembly, profileService: IProfileService, imageResizeService: IImageResizeService, animationTouchesService: IAnimationTouchesService) {
        self.presentationAssembly = presentationAssembly
        self.profileService = profileService
        self.imageResizeService = imageResizeService
        self.animationTouchesService = animationTouchesService
        super.init(nibName: "Profile", bundle: nil)
        navigationItem.title = "Editing Profile"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProfileView()
        setupDelegates()
        profileService.loadProfile()
        self.activityIndicator.hidesWhenStopped = true
    }
    
    private func setupDelegates() {
        nameTextField.delegate = self
        descriptionTextField.delegate = self
    }
    
    private func pictureFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func pictureFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func pictureFromNetwork(image: UIImage) {
        saveButtonEnabled(value: true)
        update(profile: ProfileModel(name: nil, description: nil, image: UIImagePNGRepresentation(image)))
    }
    
    private func setupProfileView() {
        self.navigationController?.view.addGestureRecognizer(UITapGestureRecognizer(target: animationTouchesService, action: #selector(animationTouchesService.handleTap(_:))))
        self.navigationController?.view.addGestureRecognizer(UILongPressGestureRecognizer(target: animationTouchesService, action: #selector(animationTouchesService.handleLongTap(_:))))
        chooseImageButton.layer.cornerRadius = 40
        chooseImageButton.imageEdgeInsets = UIEdgeInsetsMake(15,15,15,15)
        editedButton.layer.cornerRadius = 15
        editedButton.layer.borderWidth = 1
        editedButton.layer.borderColor = UIColor.black.cgColor
        userProfileImage.layer.cornerRadius = 40
        saveButton.layer.cornerRadius = 15
        createBarButton()
    }
    
    private func editingMode(value: Bool) {
        chooseImageButton.isHidden = !value
        nameTextField.isHidden = !value
        descriptionTextField.isHidden = !value
        
        staticNameLabel.isHidden = !value
        staticDescriptionLabel.isHidden = !value
        
        nameLabel.isHidden = value
        descriptionLabel.isHidden = value
        editedButton.isHidden = value
        
        saveButton.isHidden = !value
    }
    
    private func saveProfile() {
        activityIndicator.startAnimating()
        stopEditingTextFields()
        profileService.save(profile: createProfileModel())
    }
    
    private func createProfileModel() -> ProfileModel {
        guard let image = userProfileImage.image else {return ProfileModel(name: nameTextField.text, description: descriptionTextField.text, image: nil)}
        let dataImage = UIImagePNGRepresentation(image)
        let profile = ProfileModel(name: nameTextField.text, description: descriptionTextField.text, image: dataImage)
        return profile
    }
    
    private func saveButtonEnabled(value: Bool) {
        DispatchQueue.main.async {
            self.saveButton.isEnabled = value
        }
    }
    
    private func stopEditingTextFields() {
        nameTextField.endEditing(true)
        descriptionTextField.endEditing(true)
    }
    
    private func update(profile: ProfileModel) {
        if let name = profile.name {
            nameLabel.text = name
            nameTextField.text = name
        }
        
        if let description = profile.description {
            descriptionLabel.text = description
            descriptionTextField.text = description
        }
        
        guard let imgData = profile.image else {return}
        guard let image = UIImage(data: imgData) else {return}
        DispatchQueue.main.async {
            self.userProfileImage.image = self.imageResizeService.scaleImage(image: image, size: 335)
        }
    }
    
    private func createBarButton() {
        let image = UIImage(named: "CloseIcon")?.withRenderingMode(.alwaysOriginal)
        let leftButtonItem = UIBarButtonItem(
            image: image,
            style: .plain,
            target: self,
            action: #selector(closeProfile(sender: ))
        )
        self.navigationItem.leftBarButtonItem = leftButtonItem
    }
    
    @objc func closeProfile(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButtonEnabled(value: true)
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        userProfileImage.image = image
        saveButtonEnabled(value: true)
        dismiss(animated:true, completion: nil)
    }
}

extension ProfileViewController: IProfile {
    func receive(profile: ProfileModel) {
        update(profile: profile)
    }
    
    func alert(value: Bool) {
        activityIndicator.stopAnimating()
        if value == true {
            let alertController = UIAlertController(title: "Данные сохранены", message: "", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                self.profileService.loadProfile()
                self.editingMode(value: false)
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else if value == false {
            let alertController = UIAlertController(title: "Ошибка", message: "Не удалось сохранить данные", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                self.saveButtonEnabled(value: true)
            }
            let action2 = UIAlertAction(title: "Повторить", style: .default) { (action:UIAlertAction) in
                self.saveProfile()
            }
            alertController.addAction(action)
            alertController.addAction(action2)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
