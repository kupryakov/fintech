//
//  Presentation Assembly.swift
//  fintech
//
//  Created by Влад Купряков on 16/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation

protocol IPresentationAssembly {
    func conversationListViewController() -> ConversationListViewController
    func conversationViewController() -> ConversationViewController
    func profileView() -> ProfileViewController
    func imageView() -> ImageViewController
    var conversationListDelegate: IConversationList? { get set }
    var profileDelegate: IProfile? { get set }
}

class PresentationAssembly: IPresentationAssembly {
    
    private let serviceAssembly: IServicesAssembly
    
    weak var profileDelegate: IProfile?
    weak var conversationListDelegate: IConversationList?
    
    init(serviceAssembly: IServicesAssembly) {
        self.serviceAssembly = serviceAssembly
    }
    
    func conversationListViewController() -> ConversationListViewController {
        let controller = ConversationListViewController(presentationAssembly: self, conversationfetchedResultsControllerService: serviceAssembly.fetchedResultsControllerService, animationTouchesService: serviceAssembly.animationTouchesService)
        serviceAssembly.communicationServise.updateConversationListDelegate = controller.self
        conversationListDelegate = controller.self
        return controller
    }
    
    func conversationViewController() -> ConversationViewController {
        let controller = ConversationViewController(presentationAssembly: self, fetchedResultsControllerService: serviceAssembly.fetchedResultsControllerService, communicationService: serviceAssembly.communicationServise, animationTouchesService: serviceAssembly.animationTouchesService)
        serviceAssembly.communicationServise.updateConversationDelegate = controller.self
        return controller
    }
    
    func profileView() -> ProfileViewController {
        let controller = ProfileViewController(presentationAssembly: self, profileService: serviceAssembly.profileService, imageResizeService: serviceAssembly.imageResizeService, animationTouchesService: serviceAssembly.animationTouchesService)
        profileDelegate = controller.self
        serviceAssembly.profileService.delegate = controller.self
        return controller
    }
    
    func imageView() -> ImageViewController {
        let controller = ImageViewController(presentationAssembly: self, imageService: serviceAssembly.imageService, imageResizeService: serviceAssembly.imageResizeService, animationTouchesService: serviceAssembly.animationTouchesService)
        return controller
    }
}
