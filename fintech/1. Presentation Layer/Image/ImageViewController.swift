//
//  ImageViewController.swift
//  fintech
//
//  Created by Влад Купряков on 21/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var imageModel = ImageModel()
    
    private let presentationAssembly: IPresentationAssembly
    private let imageService: ILoadImageService
    private let imageResizeService: IImageResizeService
    private let animationTouchesService: IAnimationTouchesService
    
    init(presentationAssembly: IPresentationAssembly, imageService: ILoadImageService, imageResizeService: IImageResizeService, animationTouchesService: IAnimationTouchesService) {
        self.imageService = imageService
        self.presentationAssembly = presentationAssembly
        self.imageResizeService = imageResizeService
        self.animationTouchesService = animationTouchesService
        super.init(nibName: "ImageViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSetup()
        imageService.loadImageUrls(completion: ({ [weak self](imageUrls, error) in
            if let error = error {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self?.present(alert,animated: true, completion: nil)
                }
            } else if let imageUrls = imageUrls {
                self?.imageModel.imageUrls = imageUrls
                DispatchQueue.main.async {
                    self?.collectionView.reloadData()
                }
            }
        }))
    }
    
    private func collectionViewSetup() {
        self.navigationController?.view.addGestureRecognizer(UITapGestureRecognizer(target: animationTouchesService, action: #selector(animationTouchesService.handleTap(_:))))
        self.navigationController?.view.addGestureRecognizer(UILongPressGestureRecognizer(target: animationTouchesService, action: #selector(animationTouchesService.handleLongTap(_:))))
        collectionView.register(UINib(nibName: imageModel.identifier, bundle: nil), forCellWithReuseIdentifier: imageModel.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        createBarButton()
        imageModel.cellSize = (UIScreen.main.bounds.width - 20)/3.0
    }
    
    private func createBarButton() {
        let image = UIImage(named: "CloseIcon")?.withRenderingMode(.alwaysOriginal)
        let leftButtonItem = UIBarButtonItem(
            image: image,
            style: .plain,
            target: self,
            action: #selector(closeImageView(sender: ))
        )
        self.navigationItem.leftBarButtonItem = leftButtonItem
    }
    
    @objc func closeImageView(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    private func loadPreviewImage(index: Int, type: String = "preview") {
        DispatchQueue.global().async {
            if !self.imageModel.imageUrls.isEmpty {
                    self.imageService.loadImage(type: type, imageUrl: self.imageModel.imageUrls[index], completion: {[weak self](imageDict, error) in
                        if let error = error {
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                                self?.present(alert,animated: true, completion: nil)
                            }
                        } else if let imageDict = imageDict {
                            guard let fullImageUrl = imageDict.keys.first else { return }
                            guard let imageData = imageDict[fullImageUrl] else { return }
                            guard let image = UIImage(data: imageData) else { return }
                            self?.imageModel.imagesWithUrlString.append([fullImageUrl:image])
                            DispatchQueue.main.async {
                                self?.collectionView.reloadData()
                            }
                        }
                })
            }
        }
    }
    
    private func loadFullImage(index: Int, type: String = "full", completion: @escaping(() -> Void)) {
            DispatchQueue.global().async {
                self.imageService.loadImage(type: type, imageUrl: self.imageModel.imageUrls[index], completion: {[weak self](imageDict, error) in
                    if let error = error {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                            self?.present(alert,animated: true, completion: nil)
                        }
                    } else if let imageDict = imageDict {
                        guard let fullImageUrl = imageDict.keys.first else { return }
                        guard let imageData = imageDict[fullImageUrl] else { return }
                        guard let image = UIImage(data: imageData) else { return }
                        self?.imageModel.imagesWithUrlString.append([fullImageUrl:image])
                        completion()
                    }
                })
            }
    }
}

extension ImageViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         if imageModel.imageTouched == false {
                DispatchQueue.global().async {
                    self.loadFullImage(index: indexPath.row, completion: ({[weak self] in
                        guard let index = self?.imageModel.imagesWithUrlString.count else { return }
                        guard let image = self?.imageModel.imagesWithUrlString[index-1].values.first else { return }
                        self?.presentationAssembly.profileDelegate?.pictureFromNetwork(image: image)
                        self?.imageModel.imageTouched = false
                        self?.dismiss(animated: true, completion: nil)
                    }))
                }
            imageModel.imageTouched = true
        }
    }
}

extension ImageViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageModel.imageUrls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageModel.identifier, for: indexPath) as? CollectionViewCell {
            cell.activityIndicator.startAnimating()
            if imageModel.imagesWithUrlString.count > indexPath.row {
                guard let fullImageUrl = imageModel.imagesWithUrlString[indexPath.row].keys.first else { return CollectionViewCell() }
                guard let image = imageModel.imagesWithUrlString[indexPath.row][fullImageUrl] else { return CollectionViewCell() }
                cell.configureWithItem(image: imageResizeService.scaleImage(image: image, size: Int(imageModel.cellSize)))
            } else {
                if !imageModel.usedIndexes.contains(indexPath.row) {
                    loadPreviewImage(index: indexPath.row)
                    imageModel.usedIndexes.append(indexPath.row)
                }
            }
            return cell
        }
        return CollectionViewCell()
    }
}

extension ImageViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: imageModel.cellSize, height: imageModel.cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
