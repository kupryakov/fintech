//
//  CollectionViewCell.swift
//  fintech
//
//  Created by Влад Купряков on 21/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicator.hidesWhenStopped = true
    }
    
    func configureWithItem(image: UIImage) {
        self.imageView.image = image
        activityIndicator.stopAnimating()
    }
}
