//
//  ImageModel.swift
//  fintech
//
//  Created by Влад Купряков on 22/11/2018.
//  Copyright © 2018 Влад Купряков. All rights reserved.
//

import Foundation
import UIKit

struct ImageModel {
    let identifier = String(describing: CollectionViewCell.self)
    var imagesWithUrlString: [[String:UIImage]] = []
    var imageUrls: [ImageUrls] = []
    var usedIndexes: [Int] = []
    var imageTouched = false
    var cellSize: CGFloat = 0.0
}
